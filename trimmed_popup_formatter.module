<?php

/**
 * Implements hook_field_formatter_info().
 */
function trimmed_popup_formatter_field_formatter_info() {
  return [
    'text_trimmed_popup' => [
      'label' => t('Trimmed with popup'),
      'field types' => ['text', 'text_long', 'text_with_summary'],
      'settings' => [
        'trim_length' => 600,
        'view_text' => t('View all »'),
      ],
    ],
    'text_summary_or_trimmed_popup' => [
      'label' => t('Summary or trimmed with popup'),
      'field types' => ['text_with_summary'],
      'settings' => [
        'trim_length' => 600,
        'view_text' => t('View all »'),
      ],
    ],
  ];
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function trimmed_popup_formatter_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = [];

  $element['trim_length'] = array(
    '#title' => t('Trim length'),
    '#type' => 'textfield',
    '#size' => 10,
    '#default_value' => $settings['trim_length'],
    '#element_validate' => ['element_validate_integer_positive'],
    '#required' => TRUE,
  );

  $element['view_text'] = array(
    '#title' => t('View all text'),
    '#type' => 'textfield',
    '#size' => 10,
    '#default_value' => $settings['view_text'],
    '#required' => TRUE,
  );

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function trimmed_popup_formatter_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = [];

  $summary[] = t('Trim length') . ': ' . check_plain($settings['trim_length']);
  $summary[] = t('View all text') . ': "' . check_plain($settings['view_text']) . '"';

  return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function trimmed_popup_formatter_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = [];

  foreach ($items as $delta => $item) {
    $output = '';
    $output_trimmed = '';

    if ($display['type'] == 'text_trimmed_popup') {
      $output = _text_sanitize($instance, $langcode, $item, 'value');
      $output_trimmed = text_summary($output, $instance['settings']['text_processing'] ? $item['format'] : NULL, $display['settings']['trim_length']);
    }
    elseif ($display['type'] == 'text_summary_or_trimmed_popup') {
      $output = _text_sanitize($instance, $langcode, $item, 'value');

      if (!empty($item['summary'])) {
        $output_trimmed = _text_sanitize($instance, $langcode, $item, 'summary');
      }
      else {
        $output_trimmed = text_summary($output, $instance['settings']['text_processing'] ? $item['format'] : NULL, $display['settings']['trim_length']);
      }
    }

    // All elements will have at least the preview.
    $element[$delta] = [
      'preview' => [
        '#markup' => $output_trimmed,
      ],
    ];

    // If the output and trimmed are not the same. I.e. output has been trimmed,
    // add the link to view all and all content (hidden from display).
    if ($output != $output_trimmed) {
      $element[$delta] += [
        // Attach JS library to do popup.
        '#attached' => [
          'library' => [
            ['trimmed_popup_formatter', 'view'],
          ],
        ],
        'output' => [
          '#type' => 'container',
          '#attributes' => [
            'class' => ['trimmed-popup-content', 'js-hide'],
          ],
          'markup' => [
            '#markup' => $output,
          ],
        ],
        'popup' => [
          '#type' => 'link',
          '#title' => $display['settings']['view_text'],
          '#href' => '',
          '#attributes' => [
            'class' => ['trimmed-popup-link'],
          ],
        ],
      ];
    }
  }

  return $element;
}

/**
 * Implements hook_library().
 */
function trimmed_popup_formatter_library() {
  $libraries = [];

  $module_path = drupal_get_path('module', 'trimmed_popup_formatter');

  $libraries['view'] = [
    'title' => 'Trimmed popup formatter',
    'version' => '1.0.0',
    'js' => [
      "$module_path/trimmed-popup-formatter.js" => [],
    ],
    'dependencies' => [
      ['system', 'ui.dialog'],
    ],
  ];

  return $libraries;
}


