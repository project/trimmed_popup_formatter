/**
 * @file
 * Behaviour for trimmed popup field formatters.
 */

(function ($) {
  Drupal.behaviors.trimmedPopupFormatter = {

    attach: function(context, settings) {
      $('a.trimmed-popup-link', context).once().bind('click', function(event) {
        event.preventDefault();

        var $this = $(this);
        var content = $this.siblings('.trimmed-popup-content').html();

        // Show a dialog with full content.
        var $dialog = $('<div class="trimmed-popup-dialog">').append(content);

        $dialog.dialog({
          autoOpen: true,
          modal: true,
          width: 900,
          height: 600,
          position: 'center'
        });
      });
    }

  }
})(jQuery);

